package Vue;
import Modele.*;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class Dessinateur extends Canvas implements Runnable{
    public static final int WIDTH=950,HEIGHT=720;
    public static final String TITLE=" PUZZLE ";
    public int tailleCarte=5;
    public CaseEnVue level;
    public AgenceEnVue piece;
    public InfoCommun myCarteM;
    public Dessinateur(InfoCommun myCarteM) {
        Dimension dimension=new Dimension(Dessinateur.WIDTH, Dessinateur.HEIGHT);
        setPreferredSize(dimension);
        setMinimumSize(dimension);
        setMaximumSize(dimension);

        //ini ce jeu
        this.myCarteM =myCarteM;

        level = new CaseEnVue();//ini la carte
        piece= new AgenceEnVue();//ini les piece
    }

    public Dessinateur(InfoCommun myCarteM,int pix,int taille) {
        Dimension dimension=new Dimension(pix*taille, pix*taille);
        setPreferredSize(dimension);
        setMinimumSize(dimension);
        setMaximumSize(dimension);

        //ini ce jeu
        this.myCarteM =myCarteM;

        level = new CaseEnVue(pix,taille);//ini la carte
        piece= new AgenceEnVue();//ini les piece

        tailleCarte=taille;
    }
    @Override
    public void run() {
        render();
        render();
        //render();
        //regarder les detaille
        myCarteM.printMap();
    }

    private void render() {
        BufferStrategy bs=getBufferStrategy();
        if(bs==null) {
            createBufferStrategy(3);
            return;
        }

        Graphics g=bs.getDrawGraphics();
        g.setColor(Color.black);
        g.fillRect(0, 0, 720, Dessinateur.HEIGHT);
        g.setColor(Color.darkGray);
        g.fillRect(720, 0, Dessinateur.WIDTH, Dessinateur.HEIGHT);


        //print les chose
        level.render(g,myCarteM.getCarte(),myCarteM.getTaille());//appelle render la carte
        piece.render(g,myCarteM.getCarte(),myCarteM.getTaille());

        g.dispose();
        bs.show();
    }
}
