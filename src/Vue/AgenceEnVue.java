package Vue;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class AgenceEnVue extends JPanel {
    public int pix=80;
    public int taillePiece = 70;
    HashMap<Integer, Image> listImg;
    public AgenceEnVue(){
        // charge les img de piece
        listImg =new HashMap<Integer,Image>();
        listImg.put(1,Toolkit.getDefaultToolkit().getImage("src/img/1.PNG"));
        listImg.put(2,Toolkit.getDefaultToolkit().getImage("src/img/2.PNG"));
        listImg.put(3,Toolkit.getDefaultToolkit().getImage("src/img/3.PNG"));
        listImg.put(4,Toolkit.getDefaultToolkit().getImage("src/img/4.PNG"));
        listImg.put(5,Toolkit.getDefaultToolkit().getImage("src/img/5.PNG"));

        listImg.put(6,Toolkit.getDefaultToolkit().getImage("src/img/6.PNG"));
        listImg.put(7,Toolkit.getDefaultToolkit().getImage("src/img/7.PNG"));
        listImg.put(8,Toolkit.getDefaultToolkit().getImage("src/img/8.PNG"));
        listImg.put(9,Toolkit.getDefaultToolkit().getImage("src/img/9.PNG"));
        listImg.put(10,Toolkit.getDefaultToolkit().getImage("src/img/10.PNG"));

        listImg.put(11,Toolkit.getDefaultToolkit().getImage("src/img/11.PNG"));
        listImg.put(12,Toolkit.getDefaultToolkit().getImage("src/img/12.PNG"));
        listImg.put(13,Toolkit.getDefaultToolkit().getImage("src/img/13.PNG"));
        listImg.put(14,Toolkit.getDefaultToolkit().getImage("src/img/14.PNG"));
        listImg.put(15,Toolkit.getDefaultToolkit().getImage("src/img/15.PNG"));

        listImg.put(16,Toolkit.getDefaultToolkit().getImage("src/img/16.PNG"));
        listImg.put(17,Toolkit.getDefaultToolkit().getImage("src/img/17.PNG"));
        listImg.put(18,Toolkit.getDefaultToolkit().getImage("src/img/18.PNG"));
        listImg.put(19,Toolkit.getDefaultToolkit().getImage("src/img/19.PNG"));
        listImg.put(20,Toolkit.getDefaultToolkit().getImage("src/img/20.PNG"));

        listImg.put(21,Toolkit.getDefaultToolkit().getImage("src/img/21.PNG"));
        listImg.put(22,Toolkit.getDefaultToolkit().getImage("src/img/22.PNG"));
        listImg.put(23,Toolkit.getDefaultToolkit().getImage("src/img/23.PNG"));
        listImg.put(24,Toolkit.getDefaultToolkit().getImage("src/img/24.PNG"));
    }

    public AgenceEnVue(int taillePiexe){
        // charge les img de piece
        listImg =new HashMap<Integer,Image>();
        listImg.put(1,Toolkit.getDefaultToolkit().getImage("src/img/1.PNG"));
        listImg.put(2,Toolkit.getDefaultToolkit().getImage("src/img/2.PNG"));
        listImg.put(3,Toolkit.getDefaultToolkit().getImage("src/img/3.PNG"));
        listImg.put(4,Toolkit.getDefaultToolkit().getImage("src/img/4.PNG"));
        listImg.put(5,Toolkit.getDefaultToolkit().getImage("src/img/5.PNG"));

        listImg.put(6,Toolkit.getDefaultToolkit().getImage("src/img/6.PNG"));
        listImg.put(7,Toolkit.getDefaultToolkit().getImage("src/img/7.PNG"));
        listImg.put(8,Toolkit.getDefaultToolkit().getImage("src/img/8.PNG"));
        listImg.put(9,Toolkit.getDefaultToolkit().getImage("src/img/9.PNG"));
        listImg.put(10,Toolkit.getDefaultToolkit().getImage("src/img/10.PNG"));

        listImg.put(11,Toolkit.getDefaultToolkit().getImage("src/img/11.PNG"));
        listImg.put(12,Toolkit.getDefaultToolkit().getImage("src/img/12.PNG"));
        listImg.put(13,Toolkit.getDefaultToolkit().getImage("src/img/13.PNG"));
        listImg.put(14,Toolkit.getDefaultToolkit().getImage("src/img/14.PNG"));
        listImg.put(15,Toolkit.getDefaultToolkit().getImage("src/img/15.PNG"));

        listImg.put(16,Toolkit.getDefaultToolkit().getImage("src/img/16.PNG"));
        listImg.put(17,Toolkit.getDefaultToolkit().getImage("src/img/17.PNG"));
        listImg.put(18,Toolkit.getDefaultToolkit().getImage("src/img/18.PNG"));
        listImg.put(19,Toolkit.getDefaultToolkit().getImage("src/img/19.PNG"));
        listImg.put(20,Toolkit.getDefaultToolkit().getImage("src/img/20.PNG"));

        listImg.put(21,Toolkit.getDefaultToolkit().getImage("src/img/21.PNG"));
        listImg.put(22,Toolkit.getDefaultToolkit().getImage("src/img/22.PNG"));
        listImg.put(23,Toolkit.getDefaultToolkit().getImage("src/img/23.PNG"));
        listImg.put(24,Toolkit.getDefaultToolkit().getImage("src/img/24.PNG"));
        this.taillePiece=taillePiece;
    }
    public void render(Graphics g,int[][] mycarte,int tailleCarte){

        //on remplacer la carte par default
        for(int i=0;i < tailleCarte; i++){
            for(int j=0;j< tailleCarte;j++){
                if(mycarte[i][j]!=0){
                    g.drawImage(listImg.get(mycarte[i][j]), (j)*pix, (i)*pix, taillePiece, taillePiece, Color.WHITE, this);
                }
            }
        }

    }
}
