package Vue;

import javax.swing.*;
import java.awt.*;

public class CaseEnVue  extends JPanel {
    public int pix=80;
    private int height;
    private int width;
    private Image salle;
    public CaseEnVue(){
        height=pix*5;
        width=pix*5;
    }

    public CaseEnVue(int pixel,int tailleCarte){
        height=pixel*tailleCarte;
        width=pixel*tailleCarte;
        pix=pixel;
    }

    public CaseEnVue(int taille){
        height=pix*taille;
        width=pix*taille;
    }

    public int getPix() {
        return pix;
    }

    public void setPix(int pix) {
        this.pix = pix;
    }

    public void render(Graphics g, int[][] mycarte, int tailleCarte){
        //on remplacer la carte par default
        for(int x=0;x<width/pix;x++) {
            for(int y=0;y<height/pix;y++) {
                g.setColor(new Color(250,250,250));
                g.fillRect(x*pix, y*pix, 75, 75);
            }
        }
        //desssiner frontiere
        g.setColor(new Color(0,0,0));
        g.drawRect(0,0,width,height);
    }
}
