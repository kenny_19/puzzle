import Modele.Agent;
import Modele.InfoCommun;
import Modele.Position;
import Vue.Dessinateur;

import javax.swing.*;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class CaseSimple {
    public static void main(String[] args){
        //System.out.println("hello world");
        int tailleCarte = 5;
        int pix = 80;
        int nbAgence = 5;
        //objet dans memorie partage
        InfoCommun ourInfo=new InfoCommun(tailleCarte);
        //ini la carte avec taille et pixel
        Dessinateur dessine=new Dessinateur(ourInfo,pix, tailleCarte);

        JFrame frame=new JFrame();
        frame.setTitle(Dessinateur.TITLE);
        frame.add(dessine);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);

        CyclicBarrier myB = new CyclicBarrier(nbAgence,dessine);
        Thread t1;
        Thread t2;
        Thread t3;
        Thread t4;
        Thread t5;

        t1=new Agent(1,ourInfo,myB,new Position(0,0),new Position(0,4),nbAgence);
        t2=new Agent(2,ourInfo,myB,new Position(0,1),new Position(0,1),nbAgence);
        t3=new Agent(3,ourInfo,myB,new Position(0,2),new Position(0,2),nbAgence);

        t4=new Agent(4,ourInfo,myB,new Position(0,3),new Position(0,3),nbAgence);
        //echange avec 1
        t5=new Agent(5,ourInfo,myB,new Position(0,4),new Position(0,4),nbAgence);

        Executor executor = Executors.newFixedThreadPool(nbAgence);

        executor.execute(t1);
        executor.execute(t2);
        executor.execute(t3);
        executor.execute(t4);
        executor.execute(t5);


    }
}
