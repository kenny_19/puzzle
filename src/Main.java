import Modele.Agent;
import Modele.InfoCommun;
import Modele.Position;
import Vue.Dessinateur;

import javax.swing.*;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args){
        //System.out.println("hello world");
        int tailleCarte = 5;
        int pix = 80;

        int nbAgence = 24;
        //objet dans memorie partage
        InfoCommun ourInfo=new InfoCommun(tailleCarte);
        //ini la carte avec taille et pixel
        Dessinateur dessine=new Dessinateur(ourInfo,pix, tailleCarte);

        JFrame frame=new JFrame();
        frame.setTitle(Dessinateur.TITLE);
        frame.add(dessine);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);


        CyclicBarrier myB = new CyclicBarrier(nbAgence,dessine);
        Thread t1;
        Thread t2;
        Thread t3;
        Thread t4;
        Thread t5;
        Thread t6;
        Thread t7;
        Thread t8;
        Thread t9;
        Thread t10;
        Thread t11;
        Thread t12;
        Thread t13;
        Thread t14;
        Thread t15;
        Thread t16;
        Thread t17;
        Thread t18;
        Thread t19;
        Thread t20;
        Thread t21;
        Thread t22;
        Thread t23;
        Thread t24;



        t1=new Agent(1,ourInfo,myB,new Position(0,0),new Position(4,4),nbAgence);
        t2=new Agent(2,ourInfo,myB,new Position(0,1),new Position(0,1),nbAgence);
        t3=new Agent(3,ourInfo,myB,new Position(0,2),new Position(0,2),nbAgence);

        t4=new Agent(4,ourInfo,myB,new Position(0,3),new Position(0,3),nbAgence);
        //echange avec 1
        t5=new Agent(5,ourInfo,myB,new Position(0,4),new Position(0,4),nbAgence);

        t6=new Agent(6,ourInfo,myB,new Position(1,0),new Position(1,0),nbAgence);
        t7=new Agent(7,ourInfo,myB,new Position(1,1),new Position(1,1),nbAgence);
        t8=new Agent(8,ourInfo,myB,new Position(1,2),new Position(1,2),nbAgence);
        t9=new Agent(9,ourInfo,myB,new Position(1,3),new Position(1,3),nbAgence);
        t10=new Agent(10,ourInfo,myB,new Position(1,4),new Position(1,4),nbAgence);
        t11=new Agent(11,ourInfo,myB,new Position(2,0),new Position(2,0),nbAgence);
        t12=new Agent(12,ourInfo,myB,new Position(2,1),new Position(2,1),nbAgence);
        t13=new Agent(13,ourInfo,myB,new Position(2,2),new Position(2,2),nbAgence);
        t14=new Agent(14,ourInfo,myB,new Position(2,3),new Position(2,3),nbAgence);
        t15=new Agent(15,ourInfo,myB,new Position(2,4),new Position(2,4),nbAgence);
        t16=new Agent(16,ourInfo,myB,new Position(3,0),new Position(3,0),nbAgence);
        t17=new Agent(17,ourInfo,myB,new Position(3,1),new Position(3,1),nbAgence);
        t18=new Agent(18,ourInfo,myB,new Position(3,2),new Position(3,2),nbAgence);

        t19=new Agent(19,ourInfo,myB,new Position(3,3),new Position(3,3),nbAgence);
        t20=new Agent(20,ourInfo,myB,new Position(3,4),new Position(3,4),nbAgence);
        //echange avec 1
        t21=new Agent(21,ourInfo,myB,new Position(4,0),new Position(4,0),nbAgence);
        t22=new Agent(22,ourInfo,myB,new Position(4,1),new Position(4,1),nbAgence);
        t23=new Agent(23,ourInfo,myB,new Position(4,2),new Position(4,2),nbAgence);
        t24=new Agent(24,ourInfo,myB,new Position(4,3),new Position(4,3),nbAgence);




        Executor executor = Executors.newFixedThreadPool(nbAgence);


        executor.execute(t1);
        executor.execute(t2);
        executor.execute(t3);
        executor.execute(t4);
        executor.execute(t5);
        executor.execute(t6);
        executor.execute(t7);
        executor.execute(t8);
        executor.execute(t9);
        executor.execute(t10);
        executor.execute(t11);
        executor.execute(t12);
        executor.execute(t13);
        executor.execute(t14);
        executor.execute(t15);
        executor.execute(t16);
        executor.execute(t17);
        executor.execute(t18);
        executor.execute(t19);
        executor.execute(t20);
        executor.execute(t21);
        executor.execute(t22);
        executor.execute(t23);
        executor.execute(t24);

    }
}
