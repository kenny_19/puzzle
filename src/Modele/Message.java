package Modele;

public class Message {
    private int idExpediteur;
    private int idDestinateur;
    /**
     * information pour indique la d3estinateur est dns le chemin de expediteur
     * */
    private InfoPanne action;

    public Message(int idExpediteur, int idDestinqteur, InfoPanne action) {
        this.idExpediteur = idExpediteur;
        this.idDestinateur = idDestinqteur;
        this.action = action;
    }

    public Message(InfoPanne action) {
        this.action = action;
    }

    public int getIdExpediteur() {
        return idExpediteur;
    }

    public void setIdExpediteur(int idExpediteur) {
        this.idExpediteur = idExpediteur;
    }

    public int getIdDestinateur() {
        return idDestinateur;
    }

    public void setIdDestinateur(int idDestinqteur) {
        this.idDestinateur = idDestinqteur;
    }

    public InfoPanne getAction() {
        return action;
    }

    public void setAction(InfoPanne action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "Message{" +
                "idExpediteur=" + idExpediteur +
                ", idDestinateur=" + idDestinateur +
                ", action=" + action +
                '}';
    }
}
