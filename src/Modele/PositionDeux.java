package Modele;
/**
 * pour discuter case vide
 * */
public class PositionDeux {
    Position caseVideDepart;
    Position caseVideDestination;

    public PositionDeux(Position caseVideDepart, Position caseVideDestination) {
        this.caseVideDepart = caseVideDepart;
        this.caseVideDestination = caseVideDestination;
    }

    public Position getCaseVideDepart() {
        return caseVideDepart;
    }

    public void setCaseVideDepart(Position caseVideDepart) {
        this.caseVideDepart = caseVideDepart;
    }

    public Position getCaseVideDestination() {
        return caseVideDestination;
    }

    public void setCaseVideDestination(Position caseVideDestination) {
        this.caseVideDestination = caseVideDestination;
    }

    @Override
    public String toString() {
        return "PositionDeux{" +
                "caseVideDepart=" + caseVideDepart +"\n"+
                ", caseVideDestination=" + caseVideDestination +
                '}';
    }
}
