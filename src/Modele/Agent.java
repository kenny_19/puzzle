package Modele;

import Modele.InfoCommun;

import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Agent extends Thread{
    private int idAgent;
    private InfoCommun info;
    private CyclicBarrier cyclicBarrier;
    private Position positionIncedance;
    private Position positionDestination;
    private Position positionIntermediaire=null;
    private int nombreAgenceTotal=1;
    private PositionDeux caseVide=null;
    /**
     * etatChamin est l'information sur situation de deplacement.
     * 0 pas besion cherche chemin de deplacement.
     * 1 si il n'y a pas de agnece qui fait blocage dans chemin.
     * 2 si la premier case dans chemin de deplacement est bloque.
     * 3 si il y a les case block dans chemin, mias pas de premier.
     * */
    private int etatChamin=0;

    public Agent(int id,InfoCommun c,CyclicBarrier cyclicBarrier){
        this.idAgent=id;
        info = c;
        this.cyclicBarrier=cyclicBarrier;
        positionIncedance = new Position();
        positionDestination = new Position();
        //ajouter information de cette piece dans la carte commun
        placerPiece();
    }

    public Agent(int id,InfoCommun c,CyclicBarrier cyclicBarrier,Position positionIncedance,Position positionDestination){
        this.idAgent=id;
        info = c;
        this.cyclicBarrier=cyclicBarrier;
        this.positionIncedance = positionIncedance;
        this.positionDestination = positionDestination;
        //ajouter information de cette piece dans la carte commun
        placerPiece();
    }

    public Agent(int id,InfoCommun c,CyclicBarrier cyclicBarrier,Position positionIncedance,Position positionDestination,int nombreAgenceTotal){
        this.idAgent=id;
        info = c;
        this.cyclicBarrier=cyclicBarrier;
        this.positionIncedance = positionIncedance;
        this.positionDestination = positionDestination;
        this.nombreAgenceTotal=nombreAgenceTotal;
        //ajouter information de cette piece dans la carte commun
        placerPiece();
    }

    public int getidAgent() {
        return idAgent;
    }

    public void setidAgent(int id) {
        this.idAgent = id;
    }

    /**
     * tester etat de deplacement pour le chemin
     * et modifier nombreAgenceTotal
     * 0 pas besion cherche chemin de deplacement.
     * 1 si il n'y a pas de agnece qui fait blocage dans chemin.
     * 2 si la premier case dans chemin de deplacement est bloque.
     * 3 si il y a les case block dans chemin, mais pas de premier.
     * */
    public void testEtatDeplcement(){
        positionIntermediaire=null;

        //pas besion cherche chemin de deplacement.
        if(testArriver()){
            etatChamin=0;
        }else{
            ArrayList<InfoDeplacer>chemin = obtientListDeplace();
            //si il n'y a pas de agnece qui fait blocage dans chemin.
            if(chemin.size()!=0){
                etatChamin=1;
            }else{
                //tester il existe blocage dans premier case de chemin ou pas
                InfoDeplacer cheminIntermilieu = chercheCheminAuMilieu();
                //s'il y a blocage dans premier case de chemin
                if(cheminIntermilieu.getDeltaCol()==0 && cheminIntermilieu.getDeltaLigne()==0){
                    etatChamin = 2;
                    //sinon
                }else{
                    if(idAgent==1){
                        System.out.println(" id : 1 cheminIntermilieu : "+cheminIntermilieu);
                    }
                    etatChamin = 3;
                    positionIntermediaire = new Position(positionIncedance.getLng()+cheminIntermilieu.getDeltaLigne(),positionIncedance.getCol()+cheminIntermilieu.getDeltaCol());

                }
            }
        }
    }

    /**
     * tester deplacement entre lieu de depart et lieu destination
     * mais pas contient les deux points (lieu de depart et lieu destination)
     * */
    public InfoDeplacer chercheCheminAuMilieu(){
        InfoDeplacer res = new InfoDeplacer(positionIncedance);
        int destinationLng=positionDestination.getLng();
        int destinationCol=positionDestination.getCol();
        int courantLng=positionIncedance.getLng();
        int courantCol=positionIncedance.getCol();
        int [][]carte=info.getCarte();

            //case dans meme ligne
        if(destinationLng==courantLng){
            //System.out.println("courant ligne");
            //si destination a gauche
            if(destinationCol>courantCol){
                for(int i=courantCol+1;i<=destinationCol;i++){
                    if(carte[courantLng][i]!=0){
                        if(i!=courantCol+1){
                            return new InfoDeplacer(positionIncedance,0,i-1-courantCol);
                        }else{
                            break;
                        }
                    }
                }
                //si destination a droite
            }else{
                for(int i=courantCol-1;i>=destinationCol;i--){
                    if(carte[courantLng][i]!=0){
                        if(i!=courantCol-1){
                            return new InfoDeplacer(positionIncedance,0,i+1-courantCol);
                        }else{
                            break;
                        }
                    }
                }
            }
            //case dans meme colonne
        }else if(destinationCol==courantCol){
            /*
            * System.out.println("courant col");
            * */

            //si destination a haute
            if(destinationLng>courantLng){
                //System.out.println("courant col 1");
                for(int i=courantLng+1;i<=destinationLng;i++){
                    if(carte[i][courantCol]!=0){
                        if(i!=courantLng+1){
                            return new InfoDeplacer(positionIncedance,i-1-courantLng,0);
                        }else{
                            break;
                        }
                    }
                }
            }else{
                for(int i=courantLng-1;i>=destinationLng;i--){
                    if(carte[i][courantCol]!=0){
                        if(i!=courantLng-1){
                            return new InfoDeplacer(positionIncedance,i+1-courantLng,0);
                        }else{
                            break;
                        }
                    }
                }
            }

            //n'est ni dans meme ligne ni dans meme col
            //d'abord on discute que il existe lieu dans colonne intermediaire
            //s'il n'y pas de lieu dans colonne intermediaire on discute en ligne
        }else{
            //on discute que il existe lieu dans term colonne peut direc aller
            ArrayList<InfoDeplacer> cheminEnCol=InfoDeplacer.chercheChemin(this.positionIncedance,new Position(courantLng,destinationCol),info.getCarte(), info.getTaille());
            if(cheminEnCol.size()!=0){
                return cheminEnCol.get(0);
            }else{
                //on discute que il existe lieu dans term lign ligne peut direc aller
                ArrayList<InfoDeplacer> cheminEnLng=InfoDeplacer.chercheChemin(this.positionIncedance,new Position(destinationLng,courantCol),info.getCarte(), info.getTaille());
                if(cheminEnLng.size()!=0){
                    return cheminEnLng.get(0);
                }else{
                    //on discuter d'abord dans meme ligne
                    //si destination a gauche
                    if(destinationCol>courantCol){
                        for(int i=courantCol+1;i<=destinationCol;i++){
                            /*
                            * if(idAgent==1)
                                System.out.println("case 1");
                            * */
                            if(carte[courantLng][i]!=0){
                                if(i!=courantCol+1){
                                    /*
                                     * if(idAgent==1)
                                        System.out.println("lng : "+courantLng);
                                        System.out.println("col : "+i);
                                        System.out.println(" carte["+courantLng+"]["+i+"] : "+carte[courantLng][i]);
                                        System.out.println("got you 1");
                                     * */

                                    return new InfoDeplacer(positionIncedance,0,i-1-courantCol);
                                }else{
                                    break;
                                }
                            }
                        }
                        //si destination a droite
                    }else{
                        /*
                        * if(idAgent==1)
                            System.out.println("case 2");
                        * */


                        for(int i=courantCol-1;i>=destinationCol;i--){
                            if(carte[courantLng][i]!=0){
                                if(i!=courantCol-1){
                                    return new InfoDeplacer(positionIncedance,0,i+1-courantCol);
                                }else{
                                    break;
                                }
                            }
                        }
                    }
                    //on discute dans meme col
                    //si destination a haute
                    if(destinationLng>courantLng){
                        /*
                        * if(idAgent==1)
                            System.out.println("case 3");
                        * */

                        for(int i=courantLng+1;i<=destinationLng;i++){


                            if(carte[i][courantCol]!=0){
                                if(i!=courantLng+1){
                                    return new InfoDeplacer(positionIncedance,i-1-courantLng,0);
                                }else{
                                    break;
                                }
                            }
                        }
                    }else{
                        /*
                        * if(idAgent==1)
                            System.out.println("case 4");
                        * */
                        for(int i=courantLng-1;i>=destinationLng;i--){
                            if(carte[i][courantCol]!=0){
                                if(i!=courantLng-1){
                                    return new InfoDeplacer(positionIncedance,i+1-courantLng,0);
                                }else{
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        return res;
    }


    /**
     * tester si cette l'agence deja dans la destination
     * si oui renvoie ture
     * sino false
     * */
    public boolean testArriver(){
        if(positionIncedance.getCol()==positionDestination.getCol()){
            if(positionIncedance.getLng()==positionDestination.getLng()){
                return true;
            }
        }
        return false;
    }

    @Override
    public void run() {
        super.run();
        while (true) {
            try {
                cyclicBarrier.await();
                //affichierListDeplace();
                //discuter la situation dans le chemin de deplcement
                testEtatDeplcement();

                cyclicBarrier.await();
                //vider le canal sur case vide pour discuter
                info.videListCaseVide();

                cyclicBarrier.await();
                //depot une idee. pour les agents choisir une case vide pour deplacer.
                envoyerInfoSurCase();

                cyclicBarrier.await();
                //recupere le resultat au choix
                recevoirInfoCase();

                cyclicBarrier.await();
                //vider le canal sur nouveau deplacement pour discuter
                info.viderListDeplace();

                cyclicBarrier.await();
                //depot une idee. le choix de deplacement dans prochaine fois
                //les deplacement peut realiser directement (pas de blocage dans les chemin)
                envoyerInfoSurAction();

                cyclicBarrier.await();
                //recevoir l'idee de choix de deplacement dans prochaine fois
                //les deplacement peut realiser directement (pas de blocage dans les chemin)
                recevoirInfoAction();


                Thread.sleep(1000);
                cyclicBarrier.await();
                //deplacement de piece
                decaler();
                Thread.sleep(1000);
                cyclicBarrier.await();
                info.videCanal();
                cyclicBarrier.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * pour ce agence deplacer
     * Il d'abord tester
     * si il peut aller au destination directement
     * si oui,il prend les information de deplacement.
     * (chaque fois une information deplacement
     * correspond ligne ou colonne)
     * * d'apres valeurs de cheminetat
     * * 0 pas besion cherche chemin de deplacement.
     * * 1 si il n'y a pas de agnece qui fait blocage dans chemin.
     * * 2 si la premier case dans chemin de deplacement est bloque.
     *  * 3 si il y a les case block dans chemin, mais pas de premier.
     * */
    public void decaler(){
        /*
        * if(idAgent==1){
           System.out.println(" id : 1 cheminEtat : "+etatChamin);
        }
        * */
        /**
         * partie recuperation l'iformation d'action deplacement
         * */
        int[][]carte=info.getCarte();
        //on toujours prend premier l'ation dans canal d'action de deplacement
        //pour yous les case, et verififer la position sont egale ou pas
        ArrayList<InfoDeplacer> listDeplace=info.getListDeplace();
        if(listDeplace!=null && listDeplace.size()!=0){

            int indiceAction=0;
            InfoDeplacer actionDecider,tmp;
            //case special pour l'agence dans route a donne une cation d'information
            //sinon case generale
            for(int i=0;i<listDeplace.size();i++){
                tmp=listDeplace.get(i);
                //verifier il existe de case vide que on peut deplace
                if(tmp.marque && carte[tmp.getLieuCourant().getLng()][tmp.getLieuCourant().getCol()]!=0){
                    indiceAction=i;
                    break;
                }
            }

            actionDecider = listDeplace.get(indiceAction);

            if(actionDecider.getLieuCourant().getCol()==positionIncedance.getCol() && actionDecider.getLieuCourant().getLng()==positionIncedance.getLng()){
                positionDestination=new Position(actionDecider.getLieuCourant().getLng()+actionDecider.getDeltaLigne(),actionDecider.getLieuCourant().getCol()+actionDecider.getDeltaCol());
            }
        }

        /**
         * partie realiser deplacement
         * */
        //si pas dans chemin etatChemin 0
        if(!testArriver()){
            ArrayList<InfoDeplacer>chemin = obtientListDeplace();

            //si pas de blocage dans chemin etatChemin 1
            if(chemin.size()>0){
                InfoDeplacer tmp = chemin.get(0);
                int deltacol = tmp.getDeltaCol();
                int deltalng = tmp.getDeltaLigne();
                //effecer agent ancien
                effacerPiece();
                positionIncedance.setCol(positionIncedance.getCol()+deltacol);
                positionIncedance.setLng(positionIncedance.getLng()+deltalng);
                placerPiece();
            }else{
                //3 si il y a les case block dans chemin, mais pas de premier.
                if(etatChamin==3){
                    if(positionIntermediaire!=null){
                        effacerPiece();
                        positionIncedance=positionIntermediaire;
                        placerPiece();
                    }
                }
            }
        }

    }

    /**
     * placer ce agent dans la carte
     * quand on inisialiser ce piece
     * */
    public void placerPiece(){
        info.placerPiece(positionIncedance.getLng(),positionIncedance.getCol(),getidAgent());
    }

    /**
     * apres deplacement on supprime
     * l'information dans lieu ancien
     * */
    public void effacerPiece(){
        info.effacerPiece(positionIncedance.getLng(),positionIncedance.getCol(),getidAgent());
    }

    /**
     * renvoie list de deplacer du list courant vers lieu destination
     * */
    public ArrayList<InfoDeplacer> obtientListDeplace(){
        return InfoDeplacer.chercheChemin(this.positionIncedance,this.positionDestination,info.getCarte(), info.getTaille());
    }

    /**
     * renvoie list de deplacer du list courant vers lieu destination au choix (differrent que mambre valeur positionDestination)
     * */
    public ArrayList<InfoDeplacer> obtientListDeplaceSurUnLieuAuChoix(Position lieuAuchoix){
        return InfoDeplacer.chercheChemin(this.positionIncedance,lieuAuchoix,info.getCarte(), info.getTaille());
    }

    /**
     * afficher listeDeplacer
     * */
    public void affichierListDeplace(){
        ArrayList<InfoDeplacer>chemin = obtientListDeplace();
        InfoDeplacer.afficherListChemin(chemin);
    }

    /**
     * otient info pour les agence qui est dans le chemin
     * */
    public ArrayList<InfoPanne> obtientAgenceDansChemin(){
        return InfoDeplacer.chercheAgenceDansChemin(this.positionIncedance,this.positionDestination,info.getCarte(), info.getTaille());
    }

    /**
     *envoyer letter avec la information dans la memoire partage
     * si il y a un object en dans la chemin entre direction et lieu de depart
     * */
    public void envoyerInfo(){
        //tester ce agence deplace ou pas
        //si oui, il faire rien
        if(!testArriver()){
            //calculer chemin pour aller au destination
            ArrayList<InfoDeplacer>chemin = obtientListDeplace();
            //si il n'y a pas de chemin pour aller au destination il va detecter les
            //agence qui est dans le chemin
            if(chemin.size()==0){
                ArrayList<InfoPanne>listAgence = obtientAgenceDansChemin();
                //affichieListAgence(listAgence);
                for(InfoPanne i:listAgence){
                    //envoyer les message p[our les agence
                    //System.out.println(" i "+i);
                    info.depotMessage(new Message(this.idAgent,i.getIdAgence(),i));
                }
            }
        }
    }
    /**
     * chercher les case vide possible pour prochaine deplacement.
     * */
    public void envoyerInfoSurCase(){
        //suelemnt l'agence qui a etat 2 peut depot
        if(etatChamin==2){
            //if(idAgent==1)
            //   System.out.println("entree");
            Position source;
            //premier etape trouve la source(destination de case vide)
            //case pas dans meme ligne
            if(positionIncedance.getCol()!=positionDestination.getCol()){
                if(positionIncedance.getCol()<positionDestination.getCol())
                    source=new Position(positionIncedance.getLng(),positionIncedance.getCol()+1);
                else
                    source=new Position(positionIncedance.getLng(),positionIncedance.getCol()-1);
                //case dans mem ligne
            }else{
                if(positionIncedance.getLng()<positionDestination.getLng())
                    source=new Position(positionIncedance.getLng()+1,positionIncedance.getCol());
                else
                    source=new Position(positionIncedance.getLng()-1,positionIncedance.getCol());
            }

            //deuxieme etape trouve la depart de case vide
            int sourceLng = source.getLng();
            int sourceCol = source.getCol();
            int longCarte = info.getTaille();
            int [][]carte=info.getCarte();
            //calculer la plus loin destination en abs entre la bonree de kla carte et la source
            int distanceAbs = longCarte;

            Position departVideCase=null;
            boolean trouveCase=false;
            //commence board cast
            //chaque fois on cherche la domainde board cast par deux point de rectangle
            for(int i=1;i<=distanceAbs;i++){
                int gaucheHautLng = Math.max(0,sourceLng-i);
                int gaucheHautCol = Math.max(0,sourceCol-i);
                int droitBaseLng = Math.min(longCarte-1,sourceLng+i);
                int droitBaseCol = Math.min(longCarte-1,sourceCol+i);
                /*
                * if(idAgent==1){
                    System.out.println("destination de vide case");
                    System.out.println(source);
                    System.out.println("depart de vide case");
                    System.out.println(departVideCase);
                    System.out.println("**************************************");
                    System.out.println("gaucheHautLng "+gaucheHautLng);
                    System.out.println("gaucheHautCol"+gaucheHautCol);
                    System.out.println("droitBaseLng"+droitBaseLng);
                    System.out.println("droitBaseCol "+droitBaseCol);
                }
                * */
                //noouveau
                if(i==1){
                    int nbVoisin=voisinAutourAgence(carte);
                    Position p1=new Position(positionIncedance.getLng()-1,positionIncedance.getCol()-1);
                    Position p2=new Position(positionIncedance.getLng()-1,positionIncedance.getCol());
                    Position p3=new Position(positionIncedance.getLng()-1,positionIncedance.getCol()+1);
                    Position p4=new Position(positionIncedance.getLng(),positionIncedance.getCol()-1);
                    Position p5=new Position(positionIncedance.getLng(),positionIncedance.getCol()+1);
                    Position p6=new Position(positionIncedance.getLng()+1,positionIncedance.getCol()-1);
                    Position p7=new Position(positionIncedance.getLng()+1,positionIncedance.getCol());
                    Position p8=new Position(positionIncedance.getLng()+1,positionIncedance.getCol()+1);
                    //case dans meme ligne source est dans droite
                    if(positionIncedance.getLng()==source.getLng() && positionIncedance.getCol()<source.getCol()){
                        //source est dans droite
                        //System.out.println("dans ce case 1");
                            if(dansLaCarte(p8) && carte[p8.getLng()][p8.getCol()]==0){
                                departVideCase=new Position(p8.getLng(),p8.getCol());
                                trouveCase=true;
                                break;
                            }else if(dansLaCarte(p8) && dansLaCarte(p7)
                                    && carte[p7.getLng()][p7.getCol()]==0 && carte[p8.getLng()][p8.getCol()]!=0){
                                departVideCase=new Position(p7.getLng(),p7.getCol());
                                trouveCase=true;
                                break;
                            }else if(dansLaCarte(p3) && carte[p3.getLng()][p3.getCol()]==0){
                                departVideCase=new Position(p3.getLng(),p3.getCol());
                                trouveCase=true;
                                break;
                            }else if(dansLaCarte(p3) && dansLaCarte(p2)
                                    && carte[p2.getLng()][p2.getCol()]==0 && carte[p3.getLng()][p3.getCol()]!=0){
                                departVideCase=new Position(p2.getLng(),p2.getCol());
                                trouveCase=true;
                                break;
                            }



                        //case dans meme ligne source est dans gauche
                    }else if(positionIncedance.getLng()==source.getLng() && positionIncedance.getCol()>source.getCol()){
                        //System.out.println("dans ce case 2");
                       if(dansLaCarte(p6) && carte[p6.getLng()][p6.getCol()]==0){
                           departVideCase=new Position(p6.getLng(),p6.getCol());
                           trouveCase=true;
                           break;
                       }else if(dansLaCarte(p6) && dansLaCarte(p7)
                               && carte[p6.getLng()][p6.getCol()]!=0 && carte[p7.getLng()][p7.getCol()]==0){
                           departVideCase=new Position(p7.getLng(),p7.getCol());
                           trouveCase=true;
                           break;
                       }else if(dansLaCarte(p1) && carte[p1.getLng()][p1.getCol()]==0){
                           departVideCase=new Position(p1.getLng(),p1.getCol());
                           trouveCase=true;
                           break;
                       }else if(dansLaCarte(p1) && dansLaCarte(p2)
                               && carte[p1.getLng()][p1.getCol()]!=0 && carte[p2.getLng()][p2.getCol()]==0){
                           departVideCase=new Position(p2.getLng(),p2.getCol());
                           trouveCase=true;
                           break;
                       }
                        //case dans meme colonne source. source est dans haut
                    }else if(positionIncedance.getCol()==source.getCol() && positionIncedance.getLng()>source.getLng()){
                        //System.out.println("dans ce case 3");
                        if(dansLaCarte(p1) && carte[p1.getLng()][p1.getCol()]==0){
                            departVideCase=new Position(p1.getLng(),p1.getCol());
                            trouveCase=true;
                            break;
                        }else if(dansLaCarte(p1) && dansLaCarte(p4)
                                && carte[p1.getLng()][p1.getCol()]!=0 && carte[p4.getLng()][p4.getCol()]==0){
                            departVideCase=new Position(p4.getLng(),p4.getCol());
                            trouveCase=true;
                            break;
                        }else if(dansLaCarte(p3) && carte[p3.getLng()][p3.getCol()]==0){
                            departVideCase=new Position(p3.getLng(),p3.getCol());
                            trouveCase=true;
                            break;
                        }else if(dansLaCarte(p3) && dansLaCarte(p5)
                                && carte[p3.getLng()][p3.getCol()]!=0 && carte[p5.getLng()][p5.getCol()]==0){
                            departVideCase=new Position(p5.getLng(),p5.getCol());
                            trouveCase=true;
                            break;
                        }
                        //case dans meme colonne source. source est dans base
                    }else if(positionIncedance.getCol()==source.getCol() && positionIncedance.getLng()<source.getLng()){
                        //System.out.println("dans ce case 4");
                        if(dansLaCarte(p6) && carte[p6.getLng()][p6.getCol()]==0){
                            departVideCase=new Position(p6.getLng(),p6.getCol());
                            trouveCase=true;
                            break;
                        }else if(dansLaCarte(p8) && carte[p8.getLng()][p8.getCol()]==0){
                            departVideCase=new Position(p8.getLng(),p8.getCol());
                            trouveCase=true;
                            break;
                        }else if(dansLaCarte(p6) && dansLaCarte(p4) && carte[p6.getLng()][p6.getCol()]!=0 && carte[p4.getLng()][p4.getCol()]==0){
                            departVideCase=new Position(p4.getLng(),p4.getCol());
                            trouveCase=true;
                            break;
                        }else if(dansLaCarte(p8) && dansLaCarte(p5) && carte[p8.getLng()][p8.getCol()]!=0 && carte[p5.getLng()][p5.getCol()]==0){
                            departVideCase=new Position(p5.getLng(),p5.getCol());
                            trouveCase=true;
                            break;
                        }
                    }
                }

                if(trouveCase){
                    break;
                }

                //parcours 4 frontiere de rectangle
                for(int j=gaucheHautCol;j<=droitBaseCol;j++){
                    if(carte[gaucheHautLng][j]==0
                            ){
                        departVideCase=new Position(gaucheHautLng,j);
                        trouveCase=true;
                        break;
                    }
                    if(carte[droitBaseLng][j]==0
                            ){
                        departVideCase=new Position(droitBaseLng,j);
                        trouveCase=true;
                        break;
                    }
                }

                if(!trouveCase){
                    for(int j=gaucheHautLng;j<=droitBaseLng;j++){
                        if(carte[j][gaucheHautCol]==0
                                ){
                            departVideCase=new Position(j,gaucheHautCol);
                            trouveCase=true;
                            break;
                        }
                        if(carte[j][droitBaseCol]==0
                               ){
                            // &&  testVoisinACote( source,new Position(j,droitBaseCol),carte)
                            departVideCase=new Position(j,droitBaseCol);
                            trouveCase=true;
                            break;
                        }

                    }
                }


            }



            //si on deja touve la case vide On le depot
            if(departVideCase!=null){
                /*
                * System.out.println("destination de vide case");
                * System.out.println(source);
                * System.out.println("depart de vide case");
                * System.out.println(departVideCase);
                * */
                info.depotCaseVide(new PositionDeux(departVideCase,source));
            }

        }
    }
    /**
     * tester y a t-il les voisine dans cette case
     * */
    boolean testVoisinACote(Position source,Position caseVide,int[][]carte){
        Position haute=new Position(caseVide.getLng()-1,caseVide.getCol());
        Position base=new Position(caseVide.getLng()+1,caseVide.getCol());
        Position gauche=new Position(caseVide.getLng(),caseVide.getCol()-1);
        Position droite=new Position(caseVide.getLng(),caseVide.getCol()+1);
        Position tmp;
        int nbAgenceUtilisable=0;
        //case 1 voisin a cote sont pas dans carre de 3*3 au centre de l'agence dans route
        if(Math.abs(caseVide.getCol()-positionIncedance.getCol())>=2 || Math.abs(caseVide.getLng()-positionIncedance.getLng())>=2){
            tmp=haute;
            if( dansLaCarte(tmp)&& carte[tmp.getLng()][tmp.getCol()]!=0
                    && (Math.abs(tmp.getLng()-source.getLng())<Math.abs(caseVide.getLng()-source.getLng())
                            || Math.abs(tmp.getCol()-source.getCol())<Math.abs(caseVide.getCol()-source.getCol()))){
                nbAgenceUtilisable++;
            }
            tmp = base;
            if( dansLaCarte(tmp)&& carte[tmp.getLng()][tmp.getCol()]!=0
                    && (Math.abs(tmp.getLng()-source.getLng())<Math.abs(caseVide.getLng()-source.getLng())
                    || Math.abs(tmp.getCol()-source.getCol())<Math.abs(caseVide.getCol()-source.getCol()))){
                nbAgenceUtilisable++;
            }
            tmp = gauche;
            if( dansLaCarte(tmp)&& carte[tmp.getLng()][tmp.getCol()]!=0
                    && (Math.abs(tmp.getLng()-source.getLng())<Math.abs(caseVide.getLng()-source.getLng())
                    || Math.abs(tmp.getCol()-source.getCol())<Math.abs(caseVide.getCol()-source.getCol()))){
                nbAgenceUtilisable++;
            }
            tmp = droite;
            if( dansLaCarte(tmp)&& carte[tmp.getLng()][tmp.getCol()]!=0
                    && (Math.abs(tmp.getLng()-source.getLng())<Math.abs(caseVide.getLng()-source.getLng())
                    || Math.abs(tmp.getCol()-source.getCol())<Math.abs(caseVide.getCol()-source.getCol()))){
                nbAgenceUtilisable++;
            }

            if(nbAgenceUtilisable>0)
                return true;
        }
        return false;
    }

    /**
     * chercher nombre de voisin dans 3*3 pour milieu au l'agence dans route
     * */
    public int voisinAutourAgence(int[][]carte){
        Position p1=new Position(positionIncedance.getLng()-1,positionIncedance.getCol()-1);
        Position p2=new Position(positionIncedance.getLng()-1,positionIncedance.getCol());
        Position p3=new Position(positionIncedance.getLng()-1,positionIncedance.getCol()+1);
        Position p4=new Position(positionIncedance.getLng(),positionIncedance.getCol()-1);
        Position p5=new Position(positionIncedance.getLng(),positionIncedance.getCol()+1);
        Position p6=new Position(positionIncedance.getLng()+1,positionIncedance.getCol()-1);
        Position p7=new Position(positionIncedance.getLng()+1,positionIncedance.getCol());
        Position p8=new Position(positionIncedance.getLng()+1,positionIncedance.getCol()+1);
        int nbVoisin=0;
        Position tmp;
        tmp=p1;
        if(dansLaCarte(tmp) && carte[tmp.getLng()][tmp.getCol()]!=0)
            nbVoisin++;
        tmp=p2;
        if(dansLaCarte(tmp) && carte[tmp.getLng()][tmp.getCol()]!=0)
            nbVoisin++;
        tmp=p3;
        if(dansLaCarte(tmp) && carte[tmp.getLng()][tmp.getCol()]!=0)
            nbVoisin++;
        tmp=p4;
        if(dansLaCarte(tmp) && carte[tmp.getLng()][tmp.getCol()]!=0)
            nbVoisin++;
        tmp=p5;
        if(dansLaCarte(tmp) && carte[tmp.getLng()][tmp.getCol()]!=0)
            nbVoisin++;
        tmp=p6;
        if(dansLaCarte(tmp) && carte[tmp.getLng()][tmp.getCol()]!=0)
            nbVoisin++;
        tmp=p7;
        if(dansLaCarte(tmp) && carte[tmp.getLng()][tmp.getCol()]!=0)
            nbVoisin++;
        tmp=p8;
        if(dansLaCarte(tmp) && carte[tmp.getLng()][tmp.getCol()]!=0)
            nbVoisin++;
        return nbVoisin;
    }

    /**
     * tester une position sont dans la carte
     * */
    boolean dansLaCarte(Position source){
        int taille = info.getTaille();
        if(source.getCol()<=taille-1 && source.getCol()>=0){
            if(source.getLng()<=taille-1 && source.getLng()>=0){
                return true;
            }
        }
        return false;
    }

    /**
     * calculer le prochaine action possible pour prochaine deplacement.
     * d'apres l'information sur depot case vite.
     * (lieu de depart poue une case vide et lieu d'arrive pour destination)
     * */
    public void envoyerInfoSurAction(){
        if(caseVide!=null){
            Position lieuCaseVideDepart=caseVide.getCaseVideDepart();
            Position lieuCaseVideArrive=caseVide.getCaseVideDestination();
            int tailleCarte = info.getTaille();
            /**case speciale pour l'agence dans route depot une action de deplacement sur d'autre agence*/
            if(!testArriver()){
                Position positionAgenceFaitAction;
                InfoDeplacer nouvAction;

                //pour les case les trois point dans meme ligne
                if(lieuCaseVideDepart.getLng()==positionIncedance.getLng()
                        && Math.abs(lieuCaseVideDepart.getCol()-lieuCaseVideArrive.getCol())==2
                        && (lieuCaseVideDepart.getCol()+lieuCaseVideArrive.getCol())/2==positionIncedance.getCol()
                        && lieuCaseVideArrive.getLng()==positionIncedance.getLng()){
                    //l'agence dans route dans lieu plus base
                    //laisir agence de haute droite ou haut gauche dessendre une case
                    if(positionIncedance.getLng()==tailleCarte-1){

                        if(lieuCaseVideDepart.getCol()-lieuCaseVideArrive.getCol()==2)
                            positionAgenceFaitAction = new Position(positionIncedance.getLng()-1,positionIncedance.getCol()+1);
                        else
                            positionAgenceFaitAction = new Position(positionIncedance.getLng()-1,positionIncedance.getCol()-1);
                        nouvAction=new InfoDeplacer(positionAgenceFaitAction,1,0);
                        nouvAction.marque=true;
                        info.depotActionDeplacement(nouvAction);
                        //laisir agence de base droite dessendre une case
                    }else{
                        if(lieuCaseVideDepart.getCol()-lieuCaseVideArrive.getCol()==2)
                            positionAgenceFaitAction = new Position(positionIncedance.getLng()+1,positionIncedance.getCol()+1);
                        else
                            positionAgenceFaitAction = new Position(positionIncedance.getLng()+1,positionIncedance.getCol()-1);
                        nouvAction=new InfoDeplacer(positionAgenceFaitAction,-1,0);
                        nouvAction.marque=true;
                        info.depotActionDeplacement(nouvAction);
                    }
                    //pour les case les trois point dans meme colonne
                }else if(lieuCaseVideDepart.getCol()==positionIncedance.getCol()
                        && Math.abs(lieuCaseVideDepart.getLng()-lieuCaseVideArrive.getLng())==2
                        && (lieuCaseVideDepart.getLng()+lieuCaseVideArrive.getLng())/2==positionIncedance.getLng()
                        && lieuCaseVideArrive.getCol()==positionIncedance.getCol()){
                    //l'agence dans route dans lieu plus droite
                    //laisir agence de haute gauche ou base gauche aller a droite
                    if(positionIncedance.getCol()==tailleCarte-1){
                        //case base gauche
                        if(lieuCaseVideDepart.getLng()-lieuCaseVideArrive.getLng()==2)
                            positionAgenceFaitAction = new Position(positionIncedance.getLng()+1,positionIncedance.getCol()-1);
                        else
                            positionAgenceFaitAction = new Position(positionIncedance.getLng()-1,positionIncedance.getCol()-1);
                        nouvAction=new InfoDeplacer(positionAgenceFaitAction,0,1);
                        nouvAction.marque=true;
                        info.depotActionDeplacement(nouvAction);
                        //laisir agence de haute droite ou base droite aller a gauche
                    }else{
                        if(lieuCaseVideDepart.getLng()-lieuCaseVideArrive.getLng()==2)
                            positionAgenceFaitAction = new Position(positionIncedance.getLng()+1,positionIncedance.getCol()+1);
                        else
                            positionAgenceFaitAction = new Position(positionIncedance.getLng()-1,positionIncedance.getCol()+1);
                        nouvAction=new InfoDeplacer(positionAgenceFaitAction,0,-1);
                        nouvAction.marque=true;
                        info.depotActionDeplacement(nouvAction);
                    }
                    //pour les case lieuCaseVideDepart est en diagonal. quand lieuCaseVideArrive et l'agence dans route est dans meme ligne
                }else if((lieuCaseVideDepart.getCol()+lieuCaseVideArrive.getCol())/2==positionIncedance.getCol() &&
                        Math.abs(lieuCaseVideDepart.getCol()-lieuCaseVideArrive.getCol())==2 &&
                        Math.abs(lieuCaseVideDepart.getLng()-positionIncedance.getLng())==1 &&
                        Math.abs(lieuCaseVideArrive.getLng()-positionIncedance.getLng())==0){

                        //le case vide est a gauche de case dans route
                        if(lieuCaseVideDepart.getCol()<positionIncedance.getCol()){
                            if(positionIncedance.getLng()==tailleCarte-1){
                                positionAgenceFaitAction = new Position(positionIncedance.getLng()-1,positionIncedance.getCol()-1);
                            }else{
                                positionAgenceFaitAction = new Position(positionIncedance.getLng()+1,positionIncedance.getCol()-1);
                            }
                            nouvAction=new InfoDeplacer(positionAgenceFaitAction,0,-1);
                            nouvAction.marque=true;
                            info.depotActionDeplacement(nouvAction);
                            //le case vide est a droite de case dans route
                        }else{
                            if(positionIncedance.getLng()==tailleCarte-1){
                                positionAgenceFaitAction = new Position(positionIncedance.getLng()-1,positionIncedance.getCol()+1);
                            }else{
                                positionAgenceFaitAction = new Position(positionIncedance.getLng()+1,positionIncedance.getCol()+1);
                            }
                            nouvAction=new InfoDeplacer(positionAgenceFaitAction,0,1);
                            nouvAction.marque=true;
                            info.depotActionDeplacement(nouvAction);
                        }
                    //pour les case lieuCaseVideDepart est en diagonal. quand lieuCaseVideArrive et l'agence dans route est dans meme colonne
                }else if((lieuCaseVideDepart.getLng()+lieuCaseVideArrive.getLng())/2==positionIncedance.getLng()
                        && lieuCaseVideArrive.getCol()-positionIncedance.getCol()==0
                        && Math.abs(lieuCaseVideDepart.getLng()-lieuCaseVideArrive.getLng())==2
                        && Math.abs(lieuCaseVideDepart.getCol()-positionIncedance.getCol())==0){

                    //le case vide est a base de case dans route
                    if(lieuCaseVideDepart.getLng()>positionIncedance.getLng()){
                        if(positionIncedance.getCol()==tailleCarte-1){
                            positionAgenceFaitAction= new Position(positionIncedance.getLng(),positionIncedance.getCol()-1);
                        }else{
                            positionAgenceFaitAction= new Position(positionIncedance.getLng(),positionIncedance.getCol()+1);
                        }
                        nouvAction=new InfoDeplacer(positionAgenceFaitAction,1,0);
                        nouvAction.marque=true;
                        info.depotActionDeplacement(nouvAction);
                        //le case vide est a haute de case dans route
                    }else{
                        if(positionIncedance.getCol()==tailleCarte-1){
                            positionAgenceFaitAction= new Position(positionIncedance.getLng(),positionIncedance.getCol()-1);
                        }else{
                            positionAgenceFaitAction= new Position(positionIncedance.getLng(),positionIncedance.getCol()+1);
                        }
                        nouvAction=new InfoDeplacer(positionAgenceFaitAction,-1,0);
                        nouvAction.marque=true;
                        info.depotActionDeplacement(nouvAction);
                    }
                }
            }
            /**cas generale pour les agents deja dans destination*/
            if(testArriver()){
                //tester position courant est autours de lieuCaseVideDepart (en 4 dimention)
                if(lieuCaseVideDepart.getCol()==positionIncedance.getCol()){
                    if(lieuCaseVideDepart.getLng()==positionIncedance.getLng()-1 || lieuCaseVideDepart.getLng()==positionIncedance.getLng()+1){
                        int valAbs1 = Math.abs(lieuCaseVideArrive.getLng()-lieuCaseVideDepart.getLng());
                        int valAbs2 = Math.abs(lieuCaseVideArrive.getLng()-positionIncedance.getLng());
                        //si lieu d'agent est plus proche que destination de case vide alors il depot cette action
                        if(valAbs2<valAbs1){
                            if(lieuCaseVideDepart.getLng()==positionIncedance.getLng()-1){
                                info.depotActionDeplacement(new InfoDeplacer(positionIncedance,-1,0));
                            }else{
                                info.depotActionDeplacement(new InfoDeplacer(positionIncedance,1,0));
                            }
                        }
                    }
                }else if(lieuCaseVideDepart.getLng()==positionIncedance.getLng()){
                    if(lieuCaseVideDepart.getCol()==positionIncedance.getCol()-1 || lieuCaseVideDepart.getCol()==positionIncedance.getCol()+1){
                        int valAbs1 = Math.abs(lieuCaseVideArrive.getCol()-lieuCaseVideDepart.getCol());
                        int valAbs2 = Math.abs(lieuCaseVideArrive.getCol()-positionIncedance.getCol());
                        if(valAbs2<valAbs1){
                            if(lieuCaseVideDepart.getCol()==positionIncedance.getCol()-1){
                                info.depotActionDeplacement(new InfoDeplacer(positionIncedance,0,-1));
                            }else{
                                info.depotActionDeplacement(new InfoDeplacer(positionIncedance,0,1));
                            }
                        }
                    }
                }

            }

        }

    }
    /**
     * recevoir lettre qui vient de la memoire partage.
     * Si il y a ;assage ce agence doit deplacer pour lassir
     * le chemin bien fonctionne.
     * Pour differernt type de chemin il faire diffrernt action
     * */
    public void recevoirInfo(){
        Message m=info.recuMessage(this.idAgent);
        //si message existe
        if(m!=null){
            System.out.println("I am agence "+this.idAgent+"\n"+" who get msg "+m+"\n");
            InfoPanne infoDePanne = m.getAction();
            //verifer encore une fois ce agence est dans le chemain
            if(infoDePanne.getIdAgence()==this.idAgent){
                //si il block dans colonne il tester si il peut deplacer dans ligne ou pas
                if(infoDePanne.getDirectionChemin()==1){
                    int taille = info.taille;
                    boolean cheminExist;
                    for(int i=0;i<taille;i++){
                        if(i!=this.positionIncedance.getLng()){
                            cheminExist=InfoDeplacer.testerDeaplace(this.positionIncedance,new Position(positionIncedance.getLng()+i,positionIncedance.getCol()),info.getCarte(),info.taille);
                            if(cheminExist){
                                this.positionDestination = new Position(positionIncedance.getLng()+i,positionIncedance.getCol());
                                break;
                            }
                        }
                    }
                    //si il block dans ligne il tester si il peut deplacer dans colonne ou pas
                }else if(infoDePanne.getDirectionChemin()==2){
                    int taille = info.taille;
                    boolean cheminExist;
                    for(int i=0;i<taille;i++){
                        if(i!=this.positionIncedance.getLng()){
                            cheminExist=InfoDeplacer.testerDeaplace(this.positionIncedance,new Position(positionIncedance.getLng(),positionIncedance.getCol()+i),info.getCarte(),info.taille);
                            if(cheminExist){
                                this.positionDestination = new Position(positionIncedance.getLng(),positionIncedance.getCol()+i);
                                break;
                            }
                        }
                    }
                    //si il block dans carrefour il tester les deux d'abord en ligne
                }else if(infoDePanne.getDirectionChemin()==3){
                    int taille = info.taille;
                    boolean cheminExist;
                    boolean dejaChangeDsetination=false;
                    //d'abord tester dans ligne
                    for(int i=0;i<taille;i++){
                        if(i!=this.positionIncedance.getLng()){
                            cheminExist=InfoDeplacer.testerDeaplace(this.positionIncedance,new Position(positionIncedance.getLng()+i,positionIncedance.getCol()),info.getCarte(),info.taille);
                            if(cheminExist){
                                this.positionDestination = new Position(positionIncedance.getLng()+i,positionIncedance.getCol());
                                dejaChangeDsetination=true;
                                break;
                            }
                        }
                    }
                    //et puis tester dans colonne
                    if(!dejaChangeDsetination){
                        for(int i=0;i<taille;i++){
                            if(i!=this.positionIncedance.getLng()){
                                cheminExist=InfoDeplacer.testerDeaplace(this.positionIncedance,new Position(positionIncedance.getLng(),positionIncedance.getCol()+i),info.getCarte(),info.taille);
                                if(cheminExist){
                                    this.positionDestination = new Position(positionIncedance.getLng(),positionIncedance.getCol()+i);
                                    break;
                                }
                            }
                        }
                    }

                }
            }


        }
    }
    /**
     * Apres discution,agence recupere l'information sur le case vide au choix.
     * Pour cherche l'action de deplacement prochaine
     * */
    public void recevoirInfoCase(){
        caseVide=null;
        ArrayList<PositionDeux>listCaseVide=null;
        listCaseVide=info.getListCaseVide();

        //case default
        if(listCaseVide!=null && listCaseVide.size()!=0){
            caseVide=listCaseVide.get(0);
        }
    }
    /**
     * Apres discution,agence recupere l'action de deplacement dans prochaine fois.
     * */
    public void recevoirInfoAction(){
        //TODO
        if(idAgent==1){
            ArrayList<InfoDeplacer>listDeplace = info.getListDeplace();
            /*
            * System.out.println("//////////////listDeplace///////////////");
            System.out.println(listDeplace);
            System.out.println("////////////////////////////////////");
            * */
            ArrayList<PositionDeux>listCaseAuChoix=info.getListCaseVide();
            System.out.println("------------list case vide---------------");
            System.out.println(listCaseAuChoix);
            System.out.println("-----------------------------------------");
        }
    }

    /**
     * afficher list agence
     * */
    public void affichieListAgence(ArrayList<InfoPanne>listAgence){
        System.out.println("list agence : ");
        for(InfoPanne i : listAgence){
            System.out.println(i);
        }
        System.out.println("");
    }
}
