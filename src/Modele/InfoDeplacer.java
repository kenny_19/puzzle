package Modele;

import java.util.ArrayList;

/**
 * ce class formi information de deplacment
 * */
public class InfoDeplacer {
    private Position lieuCourant;
    private int deltaLigne;
    private int deltaCol;
    /**
     * pour les cas le agence dans route depot une action de deplacement sur d'autre agence
     * */
    public Boolean marque=false;
    public InfoDeplacer(Position lieu){
        lieuCourant = lieu;
        deltaLigne = 0;
        deltaCol = 0;
    }
    public InfoDeplacer(Position lieu,int deltaLigne,int deltaCol){
        lieuCourant = lieu;
        this.deltaLigne = deltaLigne;
        this.deltaCol = deltaCol;
    }

    public Position getLieuCourant() {
        return lieuCourant;
    }

    public void setLieuCourant(Position lieuCourant) {
        this.lieuCourant = lieuCourant;
    }

    public int getDeltaLigne() {
        return deltaLigne;
    }

    public void setDeltaLigne(int deltaLigne) {
        this.deltaLigne = deltaLigne;
    }

    public int getDeltaCol() {
        return deltaCol;
    }

    public void setDeltaCol(int deltaCol) {
        this.deltaCol = deltaCol;
    }

    public void affichier(){
        System.out.println(this.toString());
    }


    /**
     * generer info pour aller au destination
     * @param lieuC lieu courant
     * @param lieuF lieu destination
     * */
    public static InfoDeplacer trouverInfoDepalcer(Position lieuC,Position lieuF){
        int dx=lieuF.getLng()-lieuC.getLng();
        int dy=lieuF.getCol()-lieuC.getCol();
        return new InfoDeplacer(lieuC,dx,dy);
    }

    /**
     * generre la chemain de deplacement, vue que chaque fois l'agence choix un direction
     * puis marche dans ce direction
     * */
    public static ArrayList<InfoDeplacer> envoyerListDeplace( Position lieuC,Position lieuF,int[][]carte){
        InfoDeplacer d1,d2;
        int dx=lieuF.getLng()-lieuC.getLng();
        int dy=lieuF.getCol()-lieuC.getCol();
        ArrayList <InfoDeplacer> res = new ArrayList<>();
        if(dx!=0 && dy==0){
            res.add(trouverInfoDepalcer(lieuC,lieuF));
        }else if(dx==0 && dy!=0){
            res.add(trouverInfoDepalcer(lieuC,lieuF));
        }else if(dx!=0 && dy!=0){
            //tester depacer en ligne et puis en colonne
            if(chemin1(lieuC.getLng(),lieuC.getCol(),lieuF.getLng(),lieuF.getCol(),carte)){
                d1=new InfoDeplacer(lieuC,dx,0);
                Position tempLieu = new Position(lieuC.getLng()+dx,lieuC.getCol());
                d2=new InfoDeplacer(tempLieu,0,dy);
                res.add(d1);
                res.add(d2);
            }else if(chemin2(lieuC.getLng(),lieuC.getCol(),lieuF.getLng(),lieuF.getCol(),carte)){
                //tester depacer en colonne et puis en ligne
                d1=new InfoDeplacer(lieuC,0,dy);
                Position tempLieu = new Position(lieuC.getLng()+dx,lieuC.getCol());
                d2=new InfoDeplacer(tempLieu,dx,0);
                res.add(d1);
                res.add(d2);
            }
        }
        return res;
    }

    /**
     * cherche chemin avec la carte
     * si il n'y a pas de chemin envoyer list.size == 0
     * */
    public static ArrayList<InfoDeplacer> chercheChemin(Position lieuC,Position lieuF,int[][]carte,int taille){
        boolean cheminExist=testerDeaplace(lieuC,lieuF,carte,taille);
        if(cheminExist)
            return envoyerListDeplace(lieuC,lieuF,carte);
        //tester
        //System.out.println(cheminExist);
        return new ArrayList<>();
    }

    /**
     * cherche tous la liste de agence dans le chemin
     * si il y a le chemin entre lieu depart et destination envoyer list.size == 0
     * sinon envoyer le list qui ont les lieus et ids des agences dans le chemin
     * */
    public static ArrayList<InfoPanne> chercheAgenceDansChemin(Position lieuC,Position lieuF,int[][]carte,int taille){
        boolean cheminExist=testerDeaplace(lieuC,lieuF,carte,taille);
        if(!cheminExist)
            return envoyerListAgence(lieuC,lieuF,carte);
        return new ArrayList<>();
    }
    /**
     * cherche les lieus et ids des agences dans le chemin
     * @param lieuC lieu courant de agent qui veut passer le chemin
     * @param lieuF lieu destination de agent
     * */
    private static ArrayList<InfoPanne> envoyerListAgence(Position lieuC, Position lieuF, int[][] carte) {
        ArrayList<InfoPanne>res = new ArrayList<>();
        //case 1 chemin dans ligne
        if(lieuC.getLng()==lieuF.getLng()){
            int lng = lieuC.getLng();
            int colDepart=lieuC.getCol();
            int colDestination=lieuF.getCol();
            //discuter le destination et dans gauche ou dans droite
            if(colDepart<colDestination){
                for(int i=colDepart+1;i<=colDestination;i++){
                    if(carte[lng][i]!=0){
                        res.add(new InfoPanne(carte[lng][i],lng,i,1));
                    }
                }
            }else{
                for(int i=colDestination;i<=colDepart+1;i++){
                    if(carte[lng][i]!=0){
                        res.add(new InfoPanne(carte[lng][i],lng,i,1));
                    }
                }
            }
            //case 2 chemin dans colonne
        }else if(lieuC.getCol()==lieuF.getCol()){
            int col=lieuC.getCol();
            int lngDepart=lieuC.getLng();
            int lngDestination=lieuF.getLng();
            //discuter le destination et dans haut ou dans bas
            if(lngDepart<lngDestination){
                for(int i=lngDepart+1;i<=lngDestination;i++){
                    if(carte[i][col]!=0){
                        res.add(new InfoPanne(carte[i][col],i,col,2));
                    }
                }
            }else{
                for(int i=lngDestination;i<=lngDepart+1;i++){
                    if(carte[i][col]!=0){
                        res.add(new InfoPanne(carte[i][col],i,col,2));
                    }
                }
            }
            //case 3 le chemin contient deux partie une est dans ligne. Une est dans colonne.
            //on discuter seulment d'abord on passe a la colonne et puis passe a la ligne
        }else{
            int colDepart=lieuC.getCol();
            int colDestination=lieuF.getCol();
            int lngDepart=lieuC.getLng();
            int lngDestination=lieuF.getLng();
            //premier part discuter la destination est dans gauche ou dans droite
            if(colDepart<colDestination){
                for(int i=colDepart+1;i<=colDestination;i++){
                    if(carte[lngDepart][i]!=0){
                        //si dans carrefour
                        if(i==colDestination){
                            res.add(new InfoPanne(carte[lngDepart][i],lngDepart,i,3));
                        }else{
                            res.add(new InfoPanne(carte[lngDepart][i],lngDepart,i,1));
                        }
                    }
                }
            }else{
                for(int i=colDestination;i<=colDepart+1;i++){
                    if(carte[lngDepart][i]!=0){
                        if(i==colDestination){
                            res.add(new InfoPanne(carte[lngDepart][i],lngDepart,i,3));
                        }else{
                            res.add(new InfoPanne(carte[lngDepart][i],lngDepart,i,1));
                        }
                    }
                }
            }
            //et puis discuter la destination est dans haut ou dans base
            if(lngDepart<lngDestination){
                for(int i=lngDepart+1;i<=lngDestination;i++){
                    if(carte[i][colDestination]!=0){
                        //deja discuter carrefour donc rein faire
                        if(i!=colDestination)
                            res.add(new InfoPanne(carte[i][colDestination],i,colDestination,2));
                    }
                }
            }else{
                for(int i=lngDestination;i<=lngDepart+1;i++){
                    if(carte[i][colDestination]!=0){
                        //deja discuter carrefour donc rein faire
                        if(i!=colDestination)
                            res.add(new InfoPanne(carte[i][colDestination],i,colDestination,2));
                    }
                }
            }
        }
        return res;
    }

    /**
     * tester possible generer un chemin utile
     * si il y a agence dan le chemin et les lieu depart
     * et arrive est ordre de la carte return false
     * */
    public static boolean testerDeaplace(Position lieuC,Position lieuF,int[][]carte,int taille){
        int deblng = lieuC.getLng();
        int debcol = lieuC.getCol();
        int finlng = lieuF.getLng();
        int fincol = lieuF.getCol();
        boolean agenceExiste=false;
        //case 0 deja arrive ou hors de la carte
        //System.out.println("case01");
        if(debcol == fincol && deblng==finlng){
            return false;
        }

        //System.out.println("case02");
        if(deblng < 0 || deblng>=taille || debcol<0 ||debcol>=taille)
            return false;

        //System.out.println("case03");
        if(finlng <0 || finlng >=taille || fincol<0 || fincol >=taille)
            return false;

        int nbAgence=0;
        //case 1 deplacer dans ligne
        //System.out.println("case1");
        if(debcol == fincol){
            //si destination est en basse
            if(deblng<finlng){
                for(int lng=deblng;lng<=finlng;lng++){
                    if(carte[lng][debcol]!=0) {
                        nbAgence++;
                        if(nbAgence>1)
                            return false;
                    }
                }
                return true;
            }else{
                //si destination est en haut
                for(int lng=finlng;lng<=deblng;lng++){
                    if(carte[lng][debcol]!=0) {
                        nbAgence++;
                        if(nbAgence>1)
                            return false;
                    }
                }
                return true;
            }
        }


        //case 2 deplacer dans colonne
       // System.out.println("case2");
        if(deblng == finlng){
            //si destination est a droite
            if(debcol<fincol){
                //System.out.println("case2.1");
                for(int col=debcol;col<=fincol;col++){
                    if(carte[deblng][col]!=0){
                        nbAgence++;
                        if(nbAgence>1)
                            return false;
                    }
                }
                return true;
            }else{
                //si destination est a gauche
                //System.out.println("case2.2");
                for(int col=fincol;col<=debcol;col++){
                    if(carte[deblng][col]!=0){
                        nbAgence++;
                        if(nbAgence>1)
                            return false;
                    }
                }
                return true;
            }
        }
        //case 3 deux fois
        //3.1 depacer en ligne et npuis en colonne
        //System.out.println("case3.1");
        boolean chemin1 = chemin1(deblng,debcol,finlng,fincol,carte);
        if(chemin1)
            return true;
        //3.2 deplacer en col et puis en ligne
        //System.out.println("case3.2");
        boolean chemin2 = chemin2(deblng,debcol,finlng,fincol,carte);
        return chemin2;
    }

    /**
     * tester depacer en colonne et puis en ligne
     * */
    public static boolean chemin2(int deblng,int debcol,int finlng,int fincol,int[][]carte){
        int nbAgence = 0;
        boolean chemin2 = true;
        if(debcol<fincol){
            //si destination est a droite
            for(int col=debcol;col<=fincol;col++){
                if(carte[deblng][col]!=0){
                    nbAgence++;
                    if(nbAgence>1){
                        chemin2=false;
                        break;
                    }
                }
            }
            if(chemin2){
                //si destination est en basse
                if(deblng<finlng){
                    for(int lng=deblng;lng<=finlng;lng++){
                        if(carte[lng][fincol]!=0) {
                            nbAgence++;
                            if(nbAgence>1){
                                chemin2=false;
                                break;
                            }
                        }
                    }
                }else{
                    //si destination est en haut
                    for(int lng=finlng;lng<=deblng;lng++){
                        if(carte[lng][fincol]!=0) {
                            nbAgence++;
                            if(nbAgence>1){
                                chemin2=false;
                                break;
                            }
                        }
                    }
                }
            }
        }else{
            //si destination est a gauche
            for(int col=fincol;col<=debcol;col++){
                if(carte[deblng][col]!=0){
                    nbAgence++;
                    if(nbAgence>1){
                        chemin2=false;
                        break;
                    }
                }
            }
            if(chemin2){
                //si destination est en basse
                if(deblng<finlng){
                    for(int lng=deblng;lng<=finlng;lng++){
                        if(carte[lng][fincol]!=0) {
                            nbAgence++;
                            if(nbAgence>1){
                                chemin2=false;
                                break;
                            }
                        }
                    }
                }else{
                    //si destination est en haut
                    for(int lng=finlng;lng<=deblng;lng++){
                        if(carte[lng][fincol]!=0) {
                            nbAgence++;
                            if(nbAgence>1){
                                chemin2=false;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return chemin2;
    }

    /**
     * tester depacer en ligne et puis en colonne
     * */
    public static boolean chemin1(int deblng,int debcol,int finlng,int fincol,int[][]carte){
        boolean chemin1 = true;
        int nbAgence=0;
        if(deblng<finlng){
            for(int lng=deblng;lng<=finlng;lng++){
                if(carte[lng][debcol]!=0) {
                    nbAgence++;
                    if(nbAgence>1){
                        chemin1=false;
                        break;
                    }
                }
            }
            //si bien deplacer en lign on tester deplacer en colonne
            if(chemin1){
                //si destination est a droite
                if(debcol<fincol){
                    for(int col=debcol;col<=fincol;col++){
                        if(carte[finlng][col]!=0){
                            nbAgence++;
                            if(nbAgence>1){
                                chemin1=false;
                                break;
                            }
                        }
                    }
                }else{
                    //si destination est a gauche
                    for(int col=fincol;col<=debcol;col++){
                        if(carte[finlng][col]!=0){
                            nbAgence++;
                            if(nbAgence>1){
                                chemin1=false;
                                break;
                            }
                        }
                    }
                }
            }
            return chemin1;

        }else{
            //si destination est en haut
            for(int lng=finlng;lng<=deblng;lng++){
                if(carte[lng][debcol]!=0) {
                    nbAgence++;
                    if(nbAgence>1){
                        chemin1=false;
                        break;
                    }
                }
            }
            //si bien deplacer en lign on tester deplacer en colonne
            if(chemin1){
                //si destination est a droite
                if(debcol<fincol){
                    for(int col=debcol;col<=fincol;col++){
                        if(carte[finlng][col]!=0){
                            nbAgence++;
                            if(nbAgence>1){
                                chemin1=false;
                                break;
                            }
                        }
                    }
                }else{
                    //si destination est a gauche
                    for(int col=fincol;col<=debcol;col++){
                        if(carte[finlng][col]!=0){
                            nbAgence++;
                            if(nbAgence>1){
                                chemin1=false;
                                break;
                            }
                        }
                    }
                }
            }
            return chemin1;
        }
    }

    /**
     * afficher array list de infodeplace
     * */
    public static void afficherListChemin(ArrayList<InfoDeplacer> list){
        InfoDeplacer tmp;
        System.out.println("--------list infoDeplace--------");
        for(int i=0;i<list.size();i++){
            tmp = list.get(i);
            tmp.affichier();
        }
        System.out.println("-------------------------------");

    }

    @Override
    public String toString() {
        return "InfoDeplacer{" +
                "lieuCourant=" + lieuCourant+
                ", deltaLigne=" + deltaLigne +
                ", deltaCol=" + deltaCol +
                '}';
    }
}
