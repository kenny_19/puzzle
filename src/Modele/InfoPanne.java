package Modele;

public class InfoPanne {
    private int idAgence;
    private int ligneAgence;
    private int colAgence;
    /**
     * 1 est dans le chemin du gauche a droite
     * 2 est dans cehmin du haut a base
     * 3 est dans le carrefour de chemin
     * */
    private int directionChemin;

    public InfoPanne(int id,int x,int y,int type){
        idAgence=id;
        ligneAgence=x;
        colAgence=y;
        directionChemin=type;
    }

    public int getIdAgence() {
        return idAgence;
    }

    public void setIdAgence(int idAgence) {
        this.idAgence = idAgence;
    }

    public int getLigneAgence() {
        return ligneAgence;
    }

    public void setLigneAgence(int ligneAgence) {
        this.ligneAgence = ligneAgence;
    }

    public int getColAgence() {
        return colAgence;
    }

    public void setColAgence(int colAgence) {
        this.colAgence = colAgence;
    }

    public int getDirectionChemin() {
        return directionChemin;
    }

    public void setDirectionChemin(int directionChemin) {
        this.directionChemin = directionChemin;
    }

    @Override
    public String toString() {
        return "InfoPanne{" +
                "idAgence=" + idAgence +
                ", ligneAgence=" + ligneAgence +
                ", colAgence=" + colAgence +
                ", directionChemin=" + directionChemin +
                '}';
    }
}
