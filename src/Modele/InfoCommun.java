package Modele;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * une classe avec memoire partage entre les agences
 * */
public class InfoCommun {
    volatile int carte[][];
    volatile int taille;
    //laissir les argent envoyer les msg ver d'autre.
    volatile HashMap<Integer, Message> boiteDeLettre;
    /**
     * une list sur case vide pour decider quel case vide choix pour deplacer dans prochaine fois
     * contient une lieu depart pour le case vide depart,
     * contient une lieu dest pour le case vide destination
     */
    volatile ArrayList<PositionDeux> listCaseVide;
    /**
     * une list sur l'action de deplcacement au choix pour les agent
     * */
    volatile ArrayList<InfoDeplacer> listDeplace;


    public InfoCommun(int tailleDeCarte){
        carte = new int[tailleDeCarte][tailleDeCarte];
        taille = tailleDeCarte;
        boiteDeLettre = new HashMap<>();
    }

    public int[][] getCarte() {
        return carte;
    }

    public void setCarte(int[][] carte) {
        this.carte = carte;
    }

    public int getTaille() {
        return taille;
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }

    /**
     * ajouter un agent dans grille avec son position
     * */
    public void ajouterAgent(int col,int lng,int idAgent){
        carte[lng][col] = idAgent;
    }

    /**
     * ajouter un agent dans grille avec son position
     * */
    public void ajouterAgent(Position posAgent, int idAgent){
        ajouterAgent(posAgent.getCol(),posAgent.getLng(),idAgent);
    }

    public void afficherCarte(){
        System.out.println(" afficher la Carte");
        for(int i=0;i<taille;i++){
            for(int j=0;j<taille;j++){
                System.out.print(" "+carte[i][j]);
            }
            System.out.println();
        }
    }

    /**
     * Laissir le AGENCE se placer dans la carte
     * */
    public synchronized void placerPiece(int x,int y,int ID){
        //verifier les piece est dans la carte
        if(x<taille && x>=0 && y<=taille && y>=0){
            carte[x][y]=ID;
        }
    }

    public void printMap(){
        System.out.println("------carte---------");
        for(int i=0;i<taille;i++){
            for(int j=0;j<taille;j++){
                if(carte[i][j]<10)
                    System.out.print("0"+carte[i][j]+" ");
                else
                    System.out.print(carte[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println("---------------------");
    }

    public synchronized void effacerPiece(int x, int y, int getidAgent) {
        //verifier les piece est dans la carte
        if(x<taille && x>=0 && y<=taille && y>=0){
            carte[x][y]=0;
        }
    }

    /**
     * lasssir agence depot un message dans canal de msg
     * */
    public synchronized void depotMessage(Message m){
        boiteDeLettre.put(m.getIdDestinateur(),m);
    }

    /**
     * recevoir message
     * si il y a message on le recu
     * sinon on recu null
     * @param idDesdinateur l'agence qui recupere message
     * */
    public synchronized Message recuMessage(int idDesdinateur){
        return boiteDeLettre.get(idDesdinateur);
    }

    /**
     * vider boite lettre
     * */
    public synchronized void videCanal(){
        boiteDeLettre=new HashMap<>();
    }

    /**
     * vider listCaseVide
     * */
    public synchronized void videListCaseVide(){
        listCaseVide=new ArrayList<>();
    }

    /**
     * vider listDeplace
     * */
    public synchronized void viderListDeplace(){
        listDeplace=new ArrayList<>();
    }

    /**
     * depot la place dans canal CaseVide
     * */
    public synchronized void depotCaseVide(PositionDeux choseDepot){
        listCaseVide.add(choseDepot);
    }
    /**
     * depot action possible dans cancal listDeplace
     * */
    public synchronized void depotActionDeplacement(InfoDeplacer i){
        listDeplace.add(i);
    }

    public ArrayList<PositionDeux> getListCaseVide() {
        return listCaseVide;
    }

    public ArrayList<InfoDeplacer> getListDeplace() {
        return listDeplace;
    }
}
