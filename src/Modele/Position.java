package Modele;

import java.util.Objects;

/**
 * montrer position avec grille
 * */
public class Position {
    private int lng;
    private int col;

    public Position(int lng, int col) {
        this.lng = lng;
        this.col = col;
    }
    public Position(){
        lng=0;
        col=0;
    }

    public int getLng() {
        return lng;
    }

    public void setLng(int lng) {
        this.lng = lng;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void modifierCol(int deltaCol){
        col = col + deltaCol;
    }

    public void modifierLng(int deltaLng){
        lng = lng + deltaLng;
    }


    public void afficher(){
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return "Position{" +
                "lng=" + lng +
                ", col=" + col +
                '}';
    }
}
